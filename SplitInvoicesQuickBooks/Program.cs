﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace SplitInvoicesQuickBooks
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Enter filepath for the unsplit pdf");
            var filePath = Console.ReadLine();
            //var filePath = @"C:\Users\jhasse.LIGHTHOUSE\Documents\TestPDF\Intuit2.pdf";            
            Console.WriteLine("Enter folderpath for the output location");
            var outputPath = Console.ReadLine();
            //var outputPath = @"C:\Users\jhasse.LIGHTHOUSE\Documents\TestPDF\Output";
            if (outputPath.Substring(outputPath.Length - 1, 1) != "\\")
                outputPath += "\\";

            PdfSharp.Pdf.PdfDocument document = new PdfSharp.Pdf.PdfDocument();
            try {
                document= PdfSharp.Pdf.IO.PdfReader.Open(filePath, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error opening file. Exiting...");
                Console.WriteLine(e.Message);              
                Console.ReadKey();
                Environment.Exit(0);
            }
            PdfSharp.Pdf.PdfPages pages = document.Pages;
            List<int> pageBreaks = new List<int>();

            foreach(PdfSharp.Pdf.PdfPage pg in pages)
            {
                foreach(var t in pg.Contents)
                {
                    Console.WriteLine(t.ToString());
                }
                
            }

            PdfSharp.Pdf.PdfDocument inputDocument = document;
            PageContentItem[] contentList = new PageContentItem[document.Pages.Count];
            for(int x = 0;x<contentList.Length;x++)
            {
                contentList[x] = new PageContentItem();
            }
            String outputText="";

            //Loop through each page and extract Inv#, Bill To Name, and Matter Name
            for(int pageCount = 0;pageCount<inputDocument.PageCount;pageCount++)// (PdfSharp.Pdf.PdfPage page in inputDocument.Pages)
            {
                outputText = "";
                //Extract all Text from objects for the current page
                for (int index = 0; index < inputDocument.Pages[pageCount].Contents.Elements.Count; index++)
                {
                    PdfSharp.Pdf.PdfDictionary.PdfStream stream = inputDocument.Pages[pageCount].Contents.Elements.GetDictionary(index).Stream;
                    //Console.WriteLine();
                   // Console.WriteLine();
                    outputText += new PDFTextExtractor().ExtractTextFromPDFBytes(stream.Value);
                    //Console.WriteLine(index +"|||"+ outputText);
                }

                //Now parse the page contents 
                String newLineString = "**%NLN%**";
                String newTextBoxString = "**@TXT@**";
                String tempString;//length of 9
                List<String> textBoxContents = new List<String>();
                int textBoxCounter = 0;
                int newLineCounter = 0;
                int startText = 0;
                int endText = 0;
                for(int y = 9;y<outputText.Length;y++)
                {
                    tempString = outputText.Substring(y-9, 9);
                    if(tempString == newTextBoxString)
                    {
                        textBoxCounter++;
                        if(textBoxCounter==1)
                        {
                            startText = y;
                        }
                        else
                        {
                            endText = y - 9;
                            textBoxContents.Add(outputText.Substring(startText,endText-startText));
                            startText = y;
                        }
                    }
                }

                bool tryParse = false;
                int tempInt = 0;
                contentList[pageCount].sourceString = outputText;//Set source data for review
                for(int x = 0;x<textBoxContents.Count;x++)
                {
                    switch(x)
                    {
                        case 1:
                            if(textBoxContents[x].Length==6)
                            {
                                tryParse = int.TryParse(textBoxContents[x].Substring(0, 6), out tempInt);
                                if (tryParse)
                                    contentList[pageCount].invoiceNumber = tempInt;
                                else
                                    contentList[pageCount].invoiceNumber = -1;

                                break;

                            }
                            for(int y=9;y<textBoxContents[x].Length;y++)
                            {
                                if(textBoxContents[x].Substring(y-9,9)==newLineString || y == textBoxContents[x].Length-1)
                                {
                                    tryParse = int.TryParse(textBoxContents[x].Substring(0, y - 8), out tempInt);
                                    if (tryParse)
                                        contentList[pageCount].invoiceNumber = tempInt;
                                    else
                                        contentList[pageCount].invoiceNumber = -1;
                                    break;
                                }
                            }
                            break;                           
                        case 3:
                            for (int y = 9; y < textBoxContents[x].Length; y++)
                            {
                                if (textBoxContents[x].Substring(y - 9, 9) == newLineString || y == textBoxContents[x].Length - 1)
                                {
                                    contentList[pageCount].billToName = textBoxContents[x].Substring(0, y - 9);
                                    break;
                                }
                            }
                            break;
                        case 4:
                            if(textBoxContents[x].IndexOf(newLineString)<0 && textBoxContents[x].Length<10)
                            {
                                contentList[pageCount].matterName = textBoxContents[x];
                                break;
                            }
                            for (int y = 9; y <= textBoxContents[x].Length; y++)
                            {                                
                                if (textBoxContents[x].Substring(y - 9, 9) == newLineString)
                                {
                                    contentList[pageCount].matterName = textBoxContents[x].Substring(0, y - 9);
                                    break;
                                }
                                else if(y >= textBoxContents[x].Length)
                                {
                                    contentList[pageCount].matterName = textBoxContents[x].Substring(0, y);
                                    break;
                                }
                            }
                            break;
                    }

                }
            }

            int currentInvoice = -2;
            int firstInvoicePage = 0;
            int errorInvoices = 0;
            int succesfulInvoices = 0;
            string invoicesCreated = "";
            PdfSharp.Pdf.PdfDocument newDoc = new PdfSharp.Pdf.PdfDocument();
            string newPath = outputPath;

            for (int x = 0;x<=contentList.Length;x++)
            {
                if(x == contentList.Length || contentList[x].invoiceNumber!=currentInvoice )
                {
                    if(x!=0)
                    {
                        System.IO.Directory.CreateDirectory(newPath);
                        var tempFile = (contentList[firstInvoicePage].billToName+" - "+contentList[firstInvoicePage].matterName+" - Lighthouse Invoice "+contentList[firstInvoicePage].invoiceNumber +".pdf");
                        var tempPath = newPath;
                        invoicesCreated+="\n"+contentList[firstInvoicePage].invoiceNumber+",\"" + contentList[firstInvoicePage].billToName+"\",\""+contentList[firstInvoicePage].matterName+"\"";
                        string correctedPath = new string(tempPath
                          .Where(z => !Path.GetInvalidPathChars().Contains(z))
                          .ToArray());
                        string correctedFile = new string(tempFile
                          .Where(z => !Path.GetInvalidFileNameChars().Contains(z))
                          .ToArray());
                        newDoc.Save(correctedPath+correctedFile);
                        if (x!=contentList.Length)
                            currentInvoice = contentList[x].invoiceNumber;
                    }
                    try {
                        newDoc = new PdfSharp.Pdf.PdfDocument();
                        Console.Write("\rInvoice "+ (x+1) + "/"+contentList.Length);
                        succesfulInvoices++;
                    }
                    catch(Exception e)
                    {
                        errorInvoices++;
                        Console.WriteLine("Error on invoice# "+ contentList[firstInvoicePage].invoiceNumber);
                    }
                    firstInvoicePage = x;
                }
                if(x!=contentList.Length)
                    newDoc.AddPage(document.Pages[x]);
                
            }
            System.IO.File.WriteAllText(newPath+"\\output.txt", invoicesCreated);

            Console.WriteLine("Succesful Invoices: "+succesfulInvoices);
            Console.WriteLine("Failed Invoices: "+ errorInvoices);
            Console.WriteLine("Complete. Press any key to exit...");
            Console.ReadKey();
        }


        public static string GetElementStream(PdfSharp.Pdf.PdfPage page, int elementIndex)
        {
            string strStreamValue;
            byte[] streamValue;
            strStreamValue = "";

            if (page.Contents.Elements.Count >= elementIndex)
            {
                PdfSharp.Pdf.PdfDictionary.PdfStream stream = page.Contents.Elements.GetDictionary(elementIndex).Stream;
                streamValue = stream.Value;

                foreach (byte b in streamValue)
                {
                    strStreamValue += (char)b;
                }
            }
            return strStreamValue;
        }

        public class PageContentItem
        {
            public int invoiceNumber { get; set; }
            public String billToName { get; set; }
            public String matterName { get; set; }
            public string sourceString { get; set; }
            public PageContentItem()
            {

            }

        }
        public class PDFTextExtractor
        {
            /// BT = Beginning of a text object operator 
            /// ET = End of a text object operator
            /// Td move to the start of next line
            ///  5 Ts = superscript
            /// -5 Ts = subscript

            #region Fields

            #region _numberOfCharsToKeep
            /// <summary>
            /// The number of characters to keep, when extracting text.
            /// </summary>
            private static int _numberOfCharsToKeep = 15;
            #endregion

            #endregion



            #region ExtractTextFromPDFBytes
            /// <summary>
            /// This method processes an uncompressed Adobe (text) object 
            /// and extracts text.
            /// </summary>
            /// <param name="input">uncompressed</param>
            /// <returns></returns>
            public string ExtractTextFromPDFBytes(byte[] input)
            {
                if (input == null || input.Length == 0) return "";
                var fullString = System.Text.Encoding.ASCII.GetString(input); 
                try
                {
                    string resultString = "";

                    // Flag showing if we are we currently inside a text object
                    bool inTextObject = false;

                    // Flag showing if the next character is literal 
                    // e.g. '\\' to get a '\' character or '\(' to get '('
                    bool nextLiteral = false;

                    // () Bracket nesting level. Text appears inside ()
                    int bracketDepth = 0;

                    // Keep previous chars to get extract numbers etc.:
                    char[] previousCharacters = new char[_numberOfCharsToKeep];
                    for (int j = 0; j < _numberOfCharsToKeep; j++) previousCharacters[j] = ' ';


                    for (int i = 0; i < input.Length; i++)
                    {
                        if (i == 1850) { }
                        char c = (char)input[i];                      

                        if (inTextObject)
                        {
                            // Position the text
                            if (bracketDepth == 0)
                            {
                                if (CheckToken(new string[] { "TD", "Td" }, previousCharacters))
                                {
                                    //resultString += "\n\r";
                                    //JakeADDDD
                                    resultString += "**%NLN%**";
                                }
                                else
                                {
                                    if (CheckToken(new string[] { "'", "T*", "\"" }, previousCharacters))
                                    {
                                        //resultString += "\n";
                                        //JakeADDDD
                                        resultString += "**%NLN%**";
                                    }
                                    else
                                    {
                                        if (CheckToken(new string[] { "TJ","Tj" }, previousCharacters))
                                        {
                                            //resultString += " ";
                                            //JakeADDDD
                                            resultString += "**%NLN%**";
                                        }
                                    }
                                }
                            }

                            // End of a text object, also go to a new line.
                            if (bracketDepth == 0 &&
                                CheckToken(new string[] { "ET" }, previousCharacters))
                            {

                                inTextObject = false;
                                //resultString += " ";
                            }
                            else
                            {
                                // Start outputting text
                                if ((c == '(') && (bracketDepth == 0) && (!nextLiteral))
                                {
                                    bracketDepth = 1;
                                }
                                else
                                {
                                    // Stop outputting text
                                    if ((c == ')') && (bracketDepth == 1) && (!nextLiteral))
                                    {
                                        bracketDepth = 0;
                                    }
                                    else
                                    {
                                        // Just a normal text character:
                                        if (bracketDepth == 1)
                                        {
                                            // Only print out next character no matter what. 
                                            // Do not interpret.
                                            if (c == '\\' && !nextLiteral)
                                            {
                                                nextLiteral = true;
                                            }
                                            else
                                            {
                                                if (((c >= ' ') && (c <= '~')) ||
                                                    ((c >= 128) && (c < 255)))
                                                {
                                                    resultString += c.ToString();
                                                }

                                                nextLiteral = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Store the recent characters for 
                        // when we have to go back for a checking
                        for (int j = 0; j < _numberOfCharsToKeep - 1; j++)
                        {
                            previousCharacters[j] = previousCharacters[j + 1];
                        }
                        previousCharacters[_numberOfCharsToKeep - 1] = c;

                        // Start of a text object
                        if (!inTextObject && CheckToken(new string[] { "BT" }, previousCharacters))
                        {
                            inTextObject = true;
                            //JAKEADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                            resultString += "**@TXT@**";
                        }
                    }
                    return resultString;
                }
                catch
                {
                    return "";
                }
            }
            #endregion

            #region CheckToken
            /// <summary>
            /// Check if a certain 2 character token just came along (e.g. BT)
            /// </summary>
            /// <param name="search">the searched token</param>
            /// <param name="recent">the recent character array</param>
            /// <returns></returns>
            private bool CheckToken(string[] tokens, char[] recent)
            {
                foreach (string token in tokens)
                {
                    if (token.Length > 1)
                    {
                        if ((recent[_numberOfCharsToKeep - 3] == token[0]) &&
                            (recent[_numberOfCharsToKeep - 2] == token[1]) &&
                            ((recent[_numberOfCharsToKeep - 1] == ' ') ||
                            (recent[_numberOfCharsToKeep - 1] == 0x0d) ||
                            (recent[_numberOfCharsToKeep - 1] == 0x0a)) &&
                            ((recent[_numberOfCharsToKeep - 4] == ' ') ||
                            (recent[_numberOfCharsToKeep - 4] == 0x0d) ||
                            (recent[_numberOfCharsToKeep - 4] == 0x0a) ||
                            //JAKEADDDDDD 
                            (recent[_numberOfCharsToKeep - 4] == 93))//Checks for ']'
                            )
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                return false;
            }
            #endregion
        }
    }
}
